FROM ubuntu:16.04

USER root

ENV DEBIAN_FRONTEND noninteractive

RUN mkdir -p /var/www
RUN chmod -R 755 /var/www

RUN apt-get update
RUN apt-get purge `dpkg -l | grep php| awk '{print $2}' |tr "\n" " "`
RUN apt-get -y install software-properties-common
RUN apt-get -y install language-pack-en
RUN LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
RUN apt-get update
RUN apt-get install -y supervisor

RUN apt-get -y install php7.2-cli && apt-get clean

RUN apt-get -y install php7.2-mbstring && apt-get clean

# PHP Packages and databases
RUN apt-get -y install php7.2-fpm && apt-get clean
RUN apt-get -y install php7.2-gd && apt-get clean
RUN apt-get -y install php7.2-ldap && apt-get clean

RUN apt-get -y install php7.2-pgsql && apt-get clean
RUN apt-get -y install php7.2-sqlite && apt-get clean
RUN apt-get -y install php-pear && apt-get clean
RUN apt-get -y install php7.2-mysql && apt-get clean
RUN apt-get -y install php7.2-curl && apt-get clean
RUN apt-get -y install php7.2-cli && apt-get clean
RUN apt-get -y install php7.2-xmlrpc && apt-get clean
RUN apt-get -y install php7.2-xml && apt-get clean
RUN apt-get -y install php7.2-intl && apt-get clean
RUN apt-get -y install build-essential && apt-get clean
RUN apt-get -y install curl && apt-get clean
RUN apt-get -y install php-gettext && apt-get clean

RUN apt-get -y install php7.2-zip && apt-get clean
RUN apt-get -y install php7.2-bcmath && apt-get clean

RUN apt-get -y install php7.2-xdebug && apt-get clean
RUN apt-get -y install php7.2-igbinary && apt-get clean
RUN apt-get -y install php7.2-mongodb && apt-get clean
RUN apt-get -y install php7.2-msgpack && apt-get clean
RUN apt-get -y install php7.2-redis && apt-get clean
RUN apt-get -y install php7.2-soap && apt-get clean
RUN apt-get -y install php7.2-memcached && apt-get clean

RUN apt-get -y install apt-utils && apt-get clean
RUN apt-get -y install git && apt-get clean
RUN apt-get -y install nano && apt-get clean

# Install composer related packages.
ENV PATH $PATH:$HOME/.composer/vendor/bin
RUN cd $HOME \
    && curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer

RUN mkdir -p /var/run/php-fpm/
RUN chmod -R 755 /var/run/php-fpm/

RUN mkdir -p /run/php
RUN chmod -R 755 /run/php

RUN mkdir /entrypoint
RUN chmod -R 777 /entrypoint 

EXPOSE 9000